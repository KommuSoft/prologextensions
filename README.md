PrologExtensions
================

A set of extensions/libraries/utility functions for `Prolog`.

Contents
--------

 - The `doublenegation` library. A form of constructive negation that exploits the facts that when a branch in the SLD-tree has an even number of negations, the answers of the leaf can be copied.

Thanks
------
Thanks to Bart Demoen, for advice and being my PhD promotor.

License
-------
All software is licensed under the GNU GPL v.3 license.
