/*
  
  DoubleNegation library

  Author:         Bart Demoen and Willem Van Onsem
  E-mail:         Willem.VanOnsem@cs.kuleuven.be
  Copyright (C):  2013, KU Leuven
  Version:        0.1 (oktober 2013)

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  
*/

/**
  @descr The doublenegation library can be used to enable answer enumeration
  in the context of double negation. When using this package, the Prolog
  environment no longer uses the Negation as Finite Failure rule.
  
  @author Bart Demoen
  @author Willem Van Onsem
  @date 20/10/2013
*/

:- module(doublenegation,[system:term_expansion/2,
                          dncalln/2,
                          dncallp/2,
                          (\?)/1,
                          (\?+)/1,
                          (\*+)/1,
                          op(1150,fx,\?),
                          op(1150,fx,\?+),
                          op(1150,fx,\*+)]).
                          
:- multifile([system:term_expansion/2]).

:- dynamic([system:term_expansion/2]).

:- use_module(library(ordsets)).

:- license(gpl).

:- meta_predicate \?(0).
:- meta_predicate \?+(0).
:- meta_predicate \*+(0).
:- meta_predicate dncallp(0,+).
:- meta_predicate dncalln(0,+).
:- meta_predicate term_expansion(+,-).

:- expects_dialect(swi).

inverse(p,n).
inverse(n,p).

/**
  @form dncallp(Goal,Headers)
  @constraints
      @ground Goal
      @ground Headers
  @descr Calls the given goal in a positive context and translates the
  call to double negation logic with the given headers.
*/
dncallp(G,H) :-
  once(toPN(G,G2,p,H)),
  G2.

/**
  @form dncalln(Goal,Headers)
  @constraints
      @ground Goal
      @ground Headers
  @descr Calls the given goal in a negative context and translates the
  call to double negation logic with the given headers.
*/
dncalln(G,H) :-
  once(toPN(G,G2,n,H)),
  G2.

/**
  @form \?(Goal)
  @constraints
      @ground Goal
  @descr True if the given Goal succeeds. Regardless of the result,
  the variables are not unified.
*/
\?(Goal) :-
    \+(\+(Goal)).

/**
  @form \?+(Goal)
  @constraints
      @ground Goal
  @descr True if the given Goal fails. Regardless of the result,
  the variables are not unified.
*/
\?+(Goal) :-
    \+(Goal).

/**
  @form \*+(Goal)
  @constraints
      @ground Goal
  @descr Performs a negation only once. This is useful if a user aims to
  unify the variable, but doesn't want to backtrack over the unification.
*/
\*+(Goal) :-
    once(\+ Goal).

dncall(X) :-
    toPN(X,Y,p),
    call(Y).

toPN(C,dncallp(C,Headers),p,Headers) :-
    var(C),
    !.
toPN(C,dncalln(C,Headers),n,Headers) :-
    var(C),
    !.
toPN(call(C),dncallp(C,Headers),p,Headers) :-
    !.
toPN(call(C),dncalln(C,Headers),n,Headers) :-
    !.
toPN(\+(C1),C1M,PN,Headers) :-
    !,
    inverse(PN,PNI),
    toPN(C1,C1M,PNI,Headers).
toPN(\?(C1),\+(\+(C1)),_,_) :-
    !.
toPN(\?+(C1),\+(C1),_,_) :-
    !.
toPN(\*+(C1),once(C1M),PN,Headers) :-
    !,
    toPN(C1,C1M,PN,Headers).
toPN(A,Ap,PN,Headers) :-
    A =.. [H|T],
    ord_memberchk(H,Headers),
    !,
    atom_concat(H,PN,Hp),
    Ap =.. [Hp|T].
toPN(X,Y,PN,Headers) :-
    X =.. [XH|XT],
    predicate_property(X,meta_predicate(M)),
    !,
    M =.. [_|MT],
    toPNList(MT,XT,XTM,PN,Headers),
    Y =.. [XH|XTM].
toPN(A,A,_,_).

toPNList([],[],[],_,_) :-
    !.
toPNList([:|MT],[H|T],[H|TM],PN,Headers) :-
    !,
    toPNList(MT,T,TM,PN,Headers).
toPNList([-|MT],[H|T],[H|TM],PN,Headers) :-
    !,
    toPNList(MT,T,TM,PN,Headers).
toPNList([?|MT],[H|T],[H|TM],PN,Headers) :-
    !,
    toPNList(MT,T,TM,PN,Headers).
toPNList([+|MT],[H|T],[H|TM],PN,Headers) :-
    !,
    toPNList(MT,T,TM,PN,Headers).
toPNList([_|MT],[H|T],[HM|TM],PN,Headers) :-
    toPN(H,HM,PN,Headers),
    toPNList(MT,T,TM,PN,Headers).

rewrite(Headers,:-(H,B),:-(H,Hp),:-(Hp,Bp),:-(Hn,Bn)) :-
    !,
    callable(H),
    callable(B),
    toPN(H,Hp,p,Headers),
    toPN(B,Bp,p,Headers),
    toPN(H,Hn,n,Headers),
    toPN(B,Bn,n,Headers).

rewrite(Headers,I,:-(I,Rp),Rp,:-(An,(!,fail))) :-
    callable(I),
    toPN(I,Rp,p,Headers),
    toPN(I,An,n,Headers).

rewriteTheory(Headers,Initial,Final) :-
    rewriteTheory(Headers,Initial,O,P,N),
    append([O,P,N],Final).

rewriteTheory(_,[],[],[],[]).

rewriteTheory(Headers,[IH|IT],[OH|OT],[PH|PT],[NH|NT]) :-
    rewrite(Headers,IH,OH,PH,NH),
    rewriteTheory(Headers,IT,OT,PT,NT).

evaluate_term(end_of_file,Theory) :-
    findall(X,retract(dngrammar(X)),Theory2),
    Theory2 \= [],
    setof(X,retract(dnpredicate(X)),Headers),
    Headers \= [],
    !,
    rewriteTheory(Headers,Theory2,Theory).

evaluate_term(end_of_file,[]).

evaluate_term(:-(H,B),[]) :-
    callable(H),
    callable(B),
    !,
    H =.. [HH|_],
    assert(dngrammar(:-(H,B))),
    assert(dnpredicate(HH)).

evaluate_term(A,[]) :-
    callable(A),
    A \= (:- (_,_)),
    !,
    A =.. [HH|_],
    assert(dngrammar(A)),
    assert(dnpredicate(HH)).

evaluate_term(_,[]).

/**
  @form system:term_expansion(Term,Terms)
  @constraints
      @ground Term
      @unrestricted Terms
  @descr Translates the given term of the original theory
  into one or more new terms. In this package the generation
  of terms is postponed until the end_of_line is reached.
*/
system:term_expansion(A,B) :-
    evaluate_term(A,B).
