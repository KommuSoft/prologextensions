\documentclass{article}
\usepackage{listings,xcolor,fullpage,hyperref}
\lstset{
language=Prolog,
basicstyle=\footnotesize\tt,
numbers=left,
numberstyle=\footnotesize,
commentstyle=\rmfamily\it,
stepnumber=1,
numbersep=5pt,
backgroundcolor=\color{white},
showspaces=false,
showstringspaces=false,
showtabs=false,
frame=tb,
tabsize=2,
captionpos=b,
breaklines=true,
breakatwhitespace=false,
title=\lstname
}
\newcommand{\prolog}{\textsc{Prolog}}
\newcommand{\query}[1]{\lstinline|#1|}
\newcommand{\result}[1]{\lstinline|#1|}
\newcommand{\bigoh}[1]{\fun{\mathcal{O}}{#1}}
\newcommand{\fun}[2]{\ensuremath{#1\left(#2\right)}}
\newcommand{\predicate}[2]{\lstinline|#1/#2|}
\newcommand{\term}[1]{\lstinline|#1|}
\newcommand{\group}[1]{\left\{\begin{array}{ll}#1\end{array}\right.}
\newcommand{\ifc}[1]{\mbox{\textbf{if }}#1}
\newcommand{\otherwisec}{\mbox{\textbf{otherwise}}}
\title{Documentation for the\\\texttt{doublenegation} package\\in \prolog{}\\version 0.1}
\author{Bart Demoen \\ Willem Van Onsem}
\date{\emph{Department of Computer Science, KU Leuven}}
\begin{document}
\maketitle
\tableofcontents
\section{Introduction}
We start the documentation with a small use case. Most \prolog{} handbooks consider the family test case. Say we have a family specified with the following theory:
\lstinputlisting[lastline=10,caption=]{family.pl}
We use the closed world assumption and thus assume no other family members exist. Furthermore one can describe an orphan, a person who has no parents. We therefore can use the following predicate:
\lstinputlisting[firstline=12,lastline=13,caption=]{family.pl}
Now one can state that a person was happy in his or her childhood when he was not an orphan:
\lstinputlisting[firstline=15,lastline=16,caption=]{family.pl}
Given the defined theory, we can query if at least one family member was happy by \query{happy(\_)}. A \prolog{} environment will answer this query with \result{true.} Sometimes, one however wants to know which family members are happy. On can query the separate member with \query{happy(harry)}, \query{happy(ginny)}, etc. and different answers will appear. \prolog{} provides unification as well: in general when querying with one or more named variables, the \prolog{} system will respond by enumerating different answers. The negation mechanism of \prolog{} however doesn't allow enumeration when unification happens withing a negation since the system backtracks over such unifications.
\paragraph{}
The problems become even more severe when we introduce two new predicates called \predicate{bullied}{1} and \predicate{veryhappy}{1}:
\lstinputlisting[firstline=18,lastline=22,caption=]{family.pl}
If we now query this query asking if a family member was quite happy, with \query{veryhappy(X)}, we receive \result{false}. For the same theory however, a grounded query like \query{veryhappy(lillyluna)} succeeds.
\paragraph{}
The counterintuitive answers are due to the Negation as Finite failure system. Say we query the \predicate{veryhappy}{1} predicate with an unbounded variable, the \prolog{} environment will first search if there exists a person who is not an orphan. In order to do this, the \prolog{} environment will look for \predicate{parent}{2} facts. Since such fact exists, the query \lstinline|\+ parent(_,X)| is false and thus is the query \lstinline|\+ orphan(X)| true, the variable \term{X} however, remains unbounded. Secondly the \predicate{bullied}{1} predicate is called with a free variable, since this predicate fails finitely (\term{X} can bind with \term{harry}), therefore the second body of \predicate{verhappy}{1} fails and so does the entire predicate.
\section{Double negation}
One however can state that when the \prolog{} mechanism is an even amount of negation levels deep, one aims to search for unification and when founded the result will be positive and the even upper levels. The advantages of double negation are twofold and address the issues in the introduction.
\subsection{Negation as Finite Failure (NAF rule)}
The \emph{Negation as Finite Failure} rule was introduced in \cite{clark78} as a restricted form of ``not able to proof''. When a negative atom is called in \prolog{}, the environment aims to find a way to proof the relevant 
\paragraph{}
One can claim that the NAF rule is implemented too locally: a more broad view reveals that one who aims to prove a predicate two negations deep in the LSD-tree, actually aims to find a predicate who matches the query.
\paragraph{}
\subsection{The \texttt{doublenegation} library}
The \term{doublenegation} package transforms a given theory in order to support double negation. One simply includes the \query{:- use_module(doublenegation).} and the entire theory will be modified.
\subsection{The \emph{Odd Negation as Finite Failure}-rule and \emph{Well Founded Semantics}}
\subsection{Consequences}
As specified in the introduction, not only can one expect that the variables will unify. Some predicates who failed under the NAF-rule, will succeed under the double negation rule. This can happen due to the fact that a double negation will unify the variables in the first place and later in the execution, a negation is called with unified or at least stronger specified variables. Since a predicate with more constrained variable(s) is less likely to be proven right\footnote{Due to monotonic reasoning in definite programs.}, a negation with unified variables can succeed where a negation with ununified variables might fail.
\subsection{Restrictions}
The package has the following restrictions:
\begin{itemize}
 \item The transformation works with metapredicates in order to propagate modifications. If one defines a predicate where a goal can be used as an argument, it must be specified using the \lstinline|meta_predicate/1| predicate. If not, the execution will fall back on the default NAF rule for that part of the execution.
 \item One must follow the metarules of the built-in predicates. For instance when using the \lstinline|findall/3| method, the second argument must be a goal since the \lstinline|doublenegation/2| compiler will transform such calls as well.
\end{itemize}

\section{Negation as ununified testing}
Jon Sneyers pointed out that many \prolog{} programmers use negation in order to test a certain predicate, without unification of the variables. A query like \lstinline|\+(\+(X=Y))| for instance can be used to verify if the left part (\term{X}) can be unified with the right part (\term{Y}). In case the test succeeds, the \prolog{} environment will call the next predicate but \term{X} and \term{Y} remain unbounded\footnote{Unless of course they were already unified prior to the test.}.
\paragraph{}
In order to support these tests, we introduced two new operators: \lstinline|\?| and \lstinline|\?+|. The operators are placed in front of an atom. A query with such operators will succeed if the given atom succeeds or fails respectively. The variables however are not unified after the test is performed regardless of its outcome.
\section{Mechanism}
The package rewrites the original theory using three rules:
\begin{itemize}
 \item \textbf{Facts}:
 Given a fact $\fun{p}{\phi}$ with $\phi$ a sequence of terms, we introduce two new clauses:
 \begin{itemize}
  \item $\fun{p^+}{\phi}.$ This predicate states that the original values are true in a positive derivation.
  \item $\fun{p^-}{\phi} \leftarrow !,\mbox{fail}.$
 \end{itemize}
 \item \textbf{Clauses}: given the following clause:
 \[
  \fun{h}{\phi}\leftarrow\fun{B_1}{\nu_1},\fun{B_2}{\nu_2},\ldots,\fun{B_m}{\nu_m}.
 \]
 With $B_1,B_2,\ldots,B_m$ a sequence of atoms. We replace the clause by two new clauses:
 \[
  \left\{\begin{array}{l}
    \fun{h^+}{\phi}\leftarrow\fun{B^+_1}{\nu_1},\fun{B^+_2}{\nu_2},\ldots,\fun{B^+_m}{\nu_m}.\\
    \fun{h^-}{\phi}\leftarrow\fun{B^-_1}{\nu_1},\fun{B^-_2}{\nu_2},\ldots,\fun{B^-_m}{\nu_m}.
  \end{array}\right.
 \]
 Where the new bodies $B^+_i$ and $B^-_i$ are defined as:
 \[
  \begin{array}{cc}
   \fun{B^+_i}{\nu_i}=\group{\fun{q^+}{\nu_i}&\ifc{\fun{B_i}{\nu_i}=\fun{q_i}{\nu_i}}\\\fun{q^-}{\nu_i}&\ifc{\fun{B_i}{\nu_i}=\neg\fun{q_i}{\nu_i}}}
   \fun{B^-_i}{\nu_i}=\group{\fun{q^-}{\nu_i}&\ifc{\fun{B_i}{\nu_i}=\fun{q_i}{\nu_i}}\\\fun{q^+}{\nu_i}&\ifc{\fun{B_i}{\nu_i}=\neg\fun{q_i}{\nu_i}}}
  \end{array}
 \]
 With $q_i$ a predicate.
 \item \textbf{Original predicates}: for each predicate in the head of a clause $\fun{p}{\phi}$ we introduce a rule:
 \[
  \fun{p}{\phi}\leftarrow\fun{p^+}{\phi}
 \]
\end{itemize}
In case a predicate can take another predicate as input, a so called goal argument, the relevant arguments are rewritten as well.
\section{Performance}
The theory is modified at compile time and all introduced atoms are linked statically. Therefore there is no significant decrease of performance at runtime. At compile time, the different clauses must be translated. This is done in \bigoh{n\cdot m} with $n$ the number of items in the original theory and $m$ the number of predicates defined in the header.
\paragraph{}
One however cannot state that the modified theory will run with the same time complexity: since the execution mechanism unifies the variables, the system will backtrack over the unification in order to search for another configuration. When using the negation, the call is only executed once since the result, true or false, will remain the same. If the unification doesn't matter, one can use the \lstinline|\?| operator. In the case one wishes to unify the variables but only once, one can use the \predicate{once}{1} predicate.
\paragraph{}
The resulting theory is has a number of clauses which scales with \bigoh{2\cdot n+m} with $n$ the number of items in the original theory and $m$ the number of predicates defined in the header. This can decrease the performance due to more cache faults. In general however, a small theory won't have any overhead. Furthermore \term{call}-predicates will take more time since the environment needs to decide whether the positive or negative version of the predicate should be called. \lstinline|\?| and \lstinline|\?+| will be called when a 
\section{Public interface}
\appendix
\section{Source code}
\lstinputlisting[caption=doublenegation.pl]{../doublenegation.pl}
\section{License}
The package is distributed under the GNU General Public License 3.0.
\section{Pointers}
\begin{itemize}
 \item A \verb+git+-repository is available at \url{https://bitbucket.org/KommuSoft/prologextensions.git}.
 \item An \textbf{issue tracker} is available at \url{https://bitbucket.org/KommuSoft/prologextensions/issues}.
\end{itemize}

\section{Acknowledgements}
We would like to thank Jon Sneyers and Jo Devriendt for the interesting conversations regarding the implementation of this module.
\bibliographystyle{alpha}
\bibliography{bibliography}
\end{document}