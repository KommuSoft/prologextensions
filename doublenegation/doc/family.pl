parents(ginny,arthur,molly).
parents(jamessirius,harry,ginny).
parents(albusseverus,harry,ginny).
parents(lillyluna,harry,ginny).

parent(X,Y) :-
    parents(Y,X,_).

parent(X,Y) :-
    parents(Y,_,X).

orphan(X) :-
    \+ parent(_,X).

happy(X) :-
    \+ orphan(X).

bullied(harry).

veryhappy(X) :-
    \+ orphan(X),
    \+ bullied(X).
